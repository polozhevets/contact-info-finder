# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import urllib2
from urllib2 import urlopen
import re
from urlparse import urlparse
import time
import argparse
import sys
import socket
from socket import timeout
import httplib
import chardet
import os, sys
import json
import codecs
#from sets import Set
socket.setdefaulttimeout(10)
counts = 0
def hostname_resolves(hostname):
    try:
        socket.gethostbyname(hostname)
        return 1
    except socket.error:
        return 0

#def findall_emails():
#	emails = re.findall(r'[\w\.-]+@[\w\.-]+', str(soup))
#	return (emails)
	
	#поиск всех ссылок на сайте
def findall_links(soup):
	links = [dict(a.attrs)['href'] for a in soup('a') if 'href' in dict(a.attrs)]
	return links

#получение списка сайтов из папки
def geturlsfromfolder(path):
	
	dirs = os.listdir(path)
	sites_fromfile = []
	all_sites = []

	for sourcefile in dirs:
		print sourcefile
		with open(path+'/'+sourcefile) as site_file:
			sites_fromfile = site_file.read().splitlines()
			for url in sites_fromfile:
				if re.match('http', url):
					all_sites.append(url)
	all_sites = set(all_sites)
	print len(all_sites)
	return all_sites

#поиск внутренних ссылок на сайте с главной страницы
def find_insidelinks(links):
	
	site_links = []
	insidelinks = []

	for linkin in links:
		plink = urlparse(linkin)
		plink_d = {'scheme':plink.scheme, 'netloc':plink.netloc, 'path':plink.path, 'params':plink.params, 'query':plink.query, 'fragment':plink.fragment}
		if plink_d['netloc'] == domain.netloc:
			site_links.append(plink_d)
		if plink_d['netloc'] == '':
			plink_d['netloc'] = domain.netloc
			plink_d['scheme'] = domain.scheme
			site_links.append(plink_d)

	for url in site_links:
		insidelink = url['scheme']+'://'+url['netloc']+url['path']
			#insidelink = url['scheme']+'://'+url['netloc']+'/'+url['path']+url['params']+url['query']+url['fragment']
		insidelinks.append(str(insidelink.encode('utf-8')))

	return insidelinks

#проверка на ошибки
#поиск емейлов на всех внутренних ссылках
def findlinksofinsideemails(insidelinks):
	site_emails = []
	for insidelink_new in insidelinks:
		print 'Url:', insidelink_new
		#time.sleep(0.1)
		errors = 1
		try:
			source_new = urlopen(insidelink_new, timeout=10).read()
			encoding = chardet.detect(source_new)
			source_new = source_new.decode(encoding['encoding'])
			soup = BeautifulSoup(source_new)
			errors = 0
		except urllib2.HTTPError as e:
			if e.code != 200:
				soup = 0
				errors = 1
		except Exception as ex:
			pass
		except urllib2.HTTPError, e:
			pass
		except urllib2.URLError:
			print "Bad URL or timeout"
			pass
		except urllib2.HTTPError as e:
			if e.code == 113:
				soup = 0
				errors = 1
				print "113"
			pass
		except socket.error:
			pass
		except socket.timeout:
			pass
		except httplib.BadStatusLine:
			pass
		except UnicodeDecodeError, e:
			print e
		else:
			pass
		finally:
			pass

		if errors == 1:
			soup = 0
		else:
			pass
		
		emails = re.findall(r'[\w\.-]+@[\w\.-]+', str(soup))
	
		if  len(emails) > 0:
			emails = list(set(emails))
			for oneemail in emails:
				site_emails.append(oneemail)
		else:
			pass
	site_emails = list(set(site_emails))
	print site_emails
	return site_emails


def writeresults(site_emails):
	t_domain = str(domain.netloc)
	t_site_emails = str(site_emails)
	f.write(t_domain+t_site_emails+'\n')


def createParser ():
    parser = argparse.ArgumentParser()
    parser.add_argument ('-s', '--site', default='0')
    parser.add_argument ('-d', '--dir', default='0')
    parser.add_argument ('-pl', '--partner_link', default='0')
    return parser

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])

sites_fromfile = []

file1 = namespace.site
path = namespace.dir
partner_link = namespace.partner_link
domain = urlparse(partner_link)
print file1
print path
print partner_link+'\n'

def write_json_data_to_file(jsondata):
	with open('jsondata.txt', 'w') as outfile:
		json.dump(jsondata, outfile)

def find_partner(link):
	ucoz_partner = []
	source_new = urlopen(link, timeout=10).read()
	encoding = chardet.detect(source_new)
	source_new = source_new.decode(encoding['encoding'])
	soup = BeautifulSoup(source_new)
	p_site = soup.find(target="_blank")
	p_site = p_site.get('href')
	plink = urlparse(link)
	partner_name = soup.find(href=plink.path)
	partner_name = partner_name.get_text()
	ucoz_partner.append(partner_name, p_site, '\n')
	return ucoz_partner

def parse_ucoz():
	partner_links = []
	insidelinks = []
	site_links = []
	insidelinks_all = []
	x = 1
	while x < 60:
		insidelinks.append('http://partner.ucoz.ru/dir/0-'+str(x))
		x = x+1

	for linkin in insidelinks:
		source = urlopen(linkin, timeout=10).read()
		encoding = chardet.detect(source)
		source = source.decode(encoding['encoding'])
		soup = BeautifulSoup(source)
		links = findall_links(soup)
		insidelinks1 = find_insidelinks(links)
		for t123 in insidelinks1:
			if re.match("http://partner.ucoz.ru/dir/*", t123):
				insidelinks_all.append(t123)

	insidelinks_all = set(insidelinks_all)
	
	for tlink in insidelinks_all:
		#ucoz_partner = []
		source_new = urlopen(tlink, timeout=10).read()
		encoding = chardet.detect(source_new)
		source_new = source_new.decode(encoding['encoding'])
		soup = BeautifulSoup(source_new)
		p_site = soup.find(target="_blank")
		p_site = p_site.get('href')
		p_site = p_site.encode('utf8', 'replace')
		plink = urlparse(tlink)
		partner_name = soup.find(href=plink.path)
		partner_name = partner_name.get_text()
		partner_name = partner_name.encode('utf8', 'replace')
		#ucoz_partner.append(partner_name, p_site)
		f = open('ucoz_rez.txt', 'a')
		f.write(partner_name+' '+p_site+'\n')

	print len(insidelinks_all)

def parse_netcat():
	x=340
	insidelinks = []
	while x < 551:
		insidelinks.append('http://netcat.ru/ordersite/partners/?&curPos='+str(x))
		x = x+10

	for tlink in insidelinks:
		print tlink
		source_new = urlopen(tlink, timeout=10).read()
		encoding = chardet.detect(source_new)
		source_new = source_new.decode(encoding['encoding'])
		soup = BeautifulSoup(source_new)
		t0 = soup.find_all("div", style="height: 68px; display: inline-block;")
		for t1 in t0:
			t2 = t1.find(class_="partner_name")
			partner_name = t2.get_text()
			emails = re.findall(r'[\w\.-]+@[\w\.-]+', str(t1))
			p_links = t1.find_all("a")
			for link in p_links:
				if re.match("http:*", link.get('href')):
					p_site = link.get('href')
				if re.match("mailto:*", link.get('href')):
					email = link.get('href')
			#p_info = re.findall(r'<div>+[\w\.-]+<br>+', str(t1))
			#print p_info
			partner_name = partner_name.encode('utf8', 'replace')
			email = email.encode('utf8', 'replace')
			try:
				p_site = p_site.encode('utf8', 'replace')
			except Exception, e:
				print e
				pass
			
			f = open('netcat_rez.txt', 'a')
			try:
				f.write(partner_name+', '+p_site+', '+email+'\n')
			except Exception, e:
				print e
				pass
			except UnicodeDecodeError, e:
				print e
				pass
			
		print 'ok'

#parse_ucoz()
parse_netcat()



#data = urllib2.urlopen(partner_link)

#j = json.load(data)

#write_json_data_to_file(j)
#print 'ok'
#category_id
#lines
#current_total
#module
#per_page
#total
#method

#print j['category_id']
#for jlines in j['lines']:
#	print jlines.decode('utf-8')
#jlines = j['lines']
#print jlines['item']
#for jlinesitem in jlines['item']:

#	print jlinesitem
#print j['current_total']
#print j['module']
#print j['per_page']
#print j['total']
#print j['method']