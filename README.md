Скрипт ходит по сайтам из списка и ищет там емейлы, записывая полученные результаты в файлик. 
Пример списка сайтов в файле 'test.txt'

Script visits sites from the list and look for email adresses received results in a file. 
Example site list in the file 'test.txt'

Load site list from one file:
python find.py -s your_filename

Load side list from many files in one directory:
python find.py -d your_directory

Results into exit_your_filename.txt