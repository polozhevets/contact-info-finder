# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import urllib2
from urllib2 import urlopen
import re
from urlparse import urlparse
import time
import argparse
import sys
import socket
from socket import timeout
import httplib
import chardet
import os, sys
#from sets import Set
socket.setdefaulttimeout(10)
counts = 0
def hostname_resolves(hostname):
    try:
        socket.gethostbyname(hostname)
        return 1
    except socket.error:
        return 0

#def findall_emails():
#	emails = re.findall(r'[\w\.-]+@[\w\.-]+', str(soup))
#	return (emails)
	
	#поиск всех ссылок на сайте
def findall_links(soup):
	links = [dict(a.attrs)['href'] for a in soup('a') if 'href' in dict(a.attrs)]
	return links

#получение списка сайтов из папки
def geturlsfromfolder(path):
	
	dirs = os.listdir(path)
	sites_fromfile = []
	all_sites = []

	for sourcefile in dirs:
		print sourcefile
		with open(path+'/'+sourcefile) as site_file:
			sites_fromfile = site_file.read().splitlines()
			for url in sites_fromfile:
				if re.match('http', url):
					all_sites.append(url)
	all_sites = set(all_sites)
	print len(all_sites)
	return all_sites

#поиск внутренних ссылок на сайте с главной страницы
def find_insidelinks(links):
	
	site_links = []
	insidelinks = []

	for linkin in links:
		plink = urlparse(linkin)
		plink_d = {'scheme':plink.scheme, 'netloc':plink.netloc, 'path':plink.path, 'params':plink.params, 'query':plink.query, 'fragment':plink.fragment}
		if plink_d['netloc'] == domain.netloc:
			site_links.append(plink_d)
		if plink_d['netloc'] == '':
			plink_d['netloc'] = domain.netloc
			plink_d['scheme'] = domain.scheme
			site_links.append(plink_d)

	for url in site_links:
		if re.match("^[A-Za-z0-9_-]*$", url['path']):
			insidelink = url['scheme']+'://'+url['netloc']+'/'+url['path']
			#insidelink = url['scheme']+'://'+url['netloc']+'/'+url['path']+url['params']+url['query']+url['fragment']
			insidelinks.append(str(insidelink.encode('utf-8')))

	insidelinks = set(insidelinks)
	return insidelinks

#проверка на ошибки
#поиск емейлов на всех внутренних ссылках
def findlinksofinsideemails(insidelinks):
	site_emails = []
	for insidelink_new in insidelinks:
		print 'Url:', insidelink_new
		#time.sleep(0.1)
		errors = 1
		try:
			source_new = urlopen(insidelink_new, timeout=10).read()
			encoding = chardet.detect(source_new)
			source_new = source_new.decode(encoding['encoding'])
			soup = BeautifulSoup(source_new)
			errors = 0
		except urllib2.HTTPError as e:
			if e.code != 200:
				soup = 0
				errors = 1
		except Exception as ex:
			pass
		except urllib2.HTTPError, e:
			pass
		except urllib2.URLError:
			print "Bad URL or timeout"
			pass
		except urllib2.HTTPError as e:
			if e.code == 113:
				soup = 0
				errors = 1
				print "113"
			pass
		except socket.error:
			pass
		except socket.timeout:
			pass
		except httplib.BadStatusLine:
			pass
		except UnicodeDecodeError, e:
			print e
		else:
			pass
		finally:
			pass

		if errors == 1:
			soup = 0
		else:
			pass
		
		emails = re.findall(r'[\w\.-]+@[\w\.-]+', str(soup))
	
		if  len(emails) > 0:
			emails = list(set(emails))
			for oneemail in emails:
				site_emails.append(oneemail)
		else:
			pass
	site_emails = list(set(site_emails))
	print site_emails
	return site_emails


def writeresults(site_emails):
	t_domain = str(domain.netloc)
	t_site_emails = str(site_emails)
	f.write(t_domain+t_site_emails+'\n')


def createParser ():
    parser = argparse.ArgumentParser()
    parser.add_argument ('-s', '--site', default='0')
    parser.add_argument ('-d', '--dir', default='')
    return parser

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])

sites_fromfile = []

file1 = namespace.site
path = namespace.dir

if file1 != '0':
	f = open('exit_'+file1+'.txt', 'a')
	with open(file1) as site_file:
		sites_fromfile = site_file.read().splitlines()
else:
	sites_fromfile = geturlsfromfolder(path)
	f = open('exit_'+path+str(len(sites_fromfile))+'.txt', 'a')
	print 'sites_fromfile:', len(sites_fromfile)

for source_url in sites_fromfile:
	domain = urlparse(source_url)
	if hostname_resolves(domain.netloc) == 1:
		print 'source_url:', source_url
		try:
			source = urlopen(source_url, timeout=10).read()
			encoding = chardet.detect(source)
			source = source.decode(encoding['encoding'])
			soup = BeautifulSoup(source)
			links = findall_links(soup)
			insidelinks = find_insidelinks(links)
			site_emails = findlinksofinsideemails(insidelinks)
			print site_emails
			writeresults(site_emails)
		except urllib2.HTTPError, e:
			pass
		except urllib2.HTTPError:
			pass
		except urllib2.URLError:
			print "URLError"
			pass
		except socket.error:
			pass
		except socket.timeout:
			pass
		except httplib.BadStatusLine:
			pass
		except UnicodeDecodeError, e:
			print e
		except Exception, e:
			print e
		else:
			pass
		finally:
			pass

	else:
		print ('Unresolved host')
		pass
	counts = counts+1
	print counts

f.close()